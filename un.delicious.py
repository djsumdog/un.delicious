#!/usr/bin/env python3

import sqlite3
import json
from datetime import datetime
from os.path import isfile, join
from sys import exit, stderr
from pathlib import Path
from shutil import copy


def write_html(file, content):
    body = f"""
        <html>
        <head>
          <link rel="stylesheet" href="main.css" />
        </head>
        <body>
        <header>
          <p>
            Generated with <a href="https://battlepenguin.com/tech/???">(working title)</a>
            <a href="https://gitlab.com/djsumdog/??">Source Code / AGPLv3</a>
          </p>
        </header>
        <nav>
          Order:
          <a href="index.html">Name</a>
          <a href="date.html">Date Added</a>
          <a href="tags.html">Tags</a>
        </nav>
        {content}
        </body>
        <html>
    """
    Path('build').mkdir(parents=True, exist_ok=True)
    with open(join('build', file), 'w') as fd:
        fd.write(body)
    copy('main.css', 'build')


def write_json(bookmarks):
    print(len(bookmarks))
    with open(join('build', 'bookmarks.json'), 'w') as fd:
        json.dump(bookmarks, fd)


def sort_bookmarks_by(bookmarks, field):
    by_field_ids = [x[0] for x in sorted(bookmarks.items(), key=lambda x: x[1][field])]
    return [bookmarks[x] for x in by_field_ids]


def tag_to_id_slug(tag):
    return 'tag_' + tag.replace(" ", "_").lower()


def write_by_name(bookmarks):
    by_name = sort_bookmarks_by(bookmarks, 'name')
    write_html('index.html', bookmarks_item_html(by_name))


def write_by_date(bookmarks):
    by_date = sort_bookmarks_by(bookmarks, 'added_date')
    write_html('date.html', bookmarks_item_html(by_date))


def tags_to_html(tags):
    out = []
    for t in tags:
        out.append(f'<a href="tags.html#{tag_to_id_slug(t)}">#{t}</a>')
    return ', '.join(out)


def bookmarks_item_html(bookmarks):
    out = ''
    for bookmark in bookmarks:
        timestamp = bookmark["added_date"] // 1000000
        formatted_time = datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        tags = tags_to_html(bookmark['tags'])
        icon = f'<img class="icon" src="{bookmark["icon"]}" />' if bookmark["icon"] else ''
        mark = f'<li class="bookmark">{icon}<a href="{bookmark["url"]}"><span id="name">{bookmark["name"]}</span></a></li>'
        out += f'{mark}<li class="tags">{tags} <span class="date">({formatted_time})</span></li>'
    return f'<ul class="listing">{out}</ul>'


def write_by_tags(bookmarks):
    by_tag = {}
    for _, b in bookmarks.items():
        for tag in b['tags']:
            if tag not in by_tag:
                by_tag[tag] = []
            by_tag[tag].append(b)
    tags = sorted(by_tag)

    out = ''
    for tag in tags:
        tag_marks = bookmarks_item_html(by_tag[tag])
        slug = tag_to_id_slug(tag)
        out += f'<h3 id="{slug}">{tag}</h3>{tag_marks}'
    write_html('tags.html', out)


def run_query(sql, args=[]):
    cursor = connection.cursor()
    result = cursor.execute(sql, args)
    return cursor.fetchall()


def __get_tags():
    tag_sql = "SELECT rowid,name FROM tags"
    tags = {}
    for t in run_query(tag_sql):
        tags[t[0]] = t[1]
    return tags


def __get_bookmarks():
    bookmark_sql = """
    SELECT rowid, name, url, description, last_modified, added_date, icon FROM bookmarks
    """
    bookmarks = {}
    for b in run_query(bookmark_sql):
        bookmarks[b[0]] = {
            "name": b[1],
            "url": b[2],
            "description": b[3],
            "last_modified": b[4],
            "added_date": b[5],
            "icon": b[6] if b[6] != 'None' else None,
            "tags": []
        }
    return bookmarks


def get_bookmarks():
    bookmarks = __get_bookmarks()
    tags = __get_tags()
    bookmark_tags_sql = "SELECT bookmark_id,tag_id FROM bookmarks_tags"

    num_notfound = 0
    for bt in run_query(bookmark_tags_sql):
        bookmark_id = bt[0]
        tag_id = bt[1]

        tag = tags[tag_id]
        if bookmark_id not in bookmarks:
            print(f"Could not find bookmark {bookmark_id} to add tag {tag}")
            num_notfound += 1
            continue

        bm = bookmarks[bookmark_id]
        bookmarks[bookmark_id]['tags'].append(tags[tag_id])

    print(f"Orphan tag entries {num_notfound}")
    print("No Tags Found for the Following")
    for id, b in bookmarks.items():
        if len(b['tags']) == 0:
            print(b)

    return bookmarks


if __name__ == '__main__':
    bookmark_file = 'ybookmarks.sqlite'
    if not isfile(bookmark_file):
        print(f"Could not find #{bookmark_file}", file=stderr)
        exit(1)

    connection = sqlite3.connect('ybookmarks.sqlite')
    bookmarks = get_bookmarks()
    write_by_tags(bookmarks)
    write_by_date(bookmarks)
    write_by_name(bookmarks)
    write_json(bookmarks)

# Un.delicious

I had a sqlite file handing out in my Firefox directory for years after Delicious (social bookmark service) shut down. If you ever used the ancient Delicious Firefox plugin, you might have a `ybookmarks.sqlite` file in your Firefox profile. On Linux, it would be in your profile folder:

```
~/.mozilla/firefox/xxxxxxxx.default/ybookmarks.sqlite
```

The `undelicious.py` scrpt will read this sqlite file and output the cached bookmarks into the `build` folder as HTML, sorted by name, date and tags. It's only dependencies are Python 3 and Python's sqlite3 module.

Read the full blog post at [https://battlepenguin.com/tech/extracting-my-old-delicious-bookmarks/](https://battlepenguin.com/tech/extracting-my-old-delicious-bookmarks/).
